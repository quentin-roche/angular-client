import {async, ComponentFixture, getTestBed, TestBed} from '@angular/core/testing';

import { ShareRecipeComponent } from './share-recipe.component';
import {HttpService} from '../services/http.service';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DndComponent} from '../dnd/dnd.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {IngredientService} from '../services/ingredient.service';
import {EditRecipeService} from '../services/edit-recipe.service';
import {AuthService} from '../services/auth.service';
import {AppSettings} from '../app.settings';
import {Router} from '@angular/router';
import {AppComponent} from '../app.component';

import {MyRecipeComponent} from '../my-recipe/my-recipe.component';
import {SearchFilterPipe} from '../Pipe/searchFilter';

describe('ShareRecipeComponent', () => {
  let component: ShareRecipeComponent;
  let fixture: ComponentFixture<ShareRecipeComponent>;
  let fixtureApp: ComponentFixture<AppComponent>;


  let httpMock: HttpTestingController;
  let injector: TestBed;
  let router: Router;



  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:   [
        RouterTestingModule.withRoutes([
          { path: 'my-recipe', component: MyRecipeComponent}
        ]),
        HttpClientTestingModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        ConfirmationPopoverModule,
      ],
      providers: [
        HttpService,
        IngredientService,
        EditRecipeService,
        AuthService
      ],
      declarations: [ ShareRecipeComponent, DndComponent, MyRecipeComponent, SearchFilterPipe ]
    })
    .compileComponents();






  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init form', () => {
    component.ngOnInit();

    expect(component.userForm.value.name).toBe('');
    expect(component.userForm.value.ingredients[0].name).toBe('');
    expect(component.userForm.value.steps[0].text).toBe('');
  });

  it('should pathValue', () => {
    component.ngOnInit();
    component.recipeId = 'recipe_id';
    component.myRecipes = [{_id: 'recipe_id',
      name: 'name_recipe',
      ingredients: [{name: 'name_ingredient0'}, {name: 'name_ingredient1'}],
      steps: [{text: 'text_step0'}, {text: 'text_step1'}]}];
    component.patchValues();
    expect(component.userForm.value.name).toBe('name_recipe');
    expect(component.userForm.value.ingredients[0].name).toBe('name_ingredient0');
    expect(component.userForm.value.ingredients[1].name).toBe('name_ingredient1');
    expect(component.userForm.value.steps[0].text).toBe('text_step0');
    expect(component.userForm.value.steps[1].text).toBe('text_step1');
  });

  it('should get state', () => {
    component.ngOnInit();
    component.recipeId = 'recipe_id';
    component.myRecipes = [{_id: 'recipe_id',
      name: 'name_recipe',
      ingredients: [{name: 'name_ingredient0'}, {name: 'name_ingredient1'}],
      steps: [{text: 'text_step0'}, {text: 'text_step1'}]}];
    component.patchValues();
    expect(component.userForm.value.name).toBe('name_recipe');
    expect(component.userForm.value.ingredients[0].name).toBe('name_ingredient0');
    expect(component.userForm.value.ingredients[1].name).toBe('name_ingredient1');
    expect(component.userForm.value.steps[0].text).toBe('text_step0');
    expect(component.userForm.value.steps[1].text).toBe('text_step1');
  });

  it('should save', () => {
    component.ngOnInit();
    component.recipeId = 'recipe_id';
    component.myRecipes = [{_id: 'recipe_id',
      name: 'name_recipe',
      ingredients: [{name: 'name_ingredient0'}, {name: 'name_ingredient1'}],
      steps: [{text: 'text_step0'}, {text: 'text_step1'}]}];
    component.patchValues();
    component.onSave();



    const req = httpMock.expectOne(AppSettings.url_API + AppSettings.api.edit_recipe.create.change);
    expect(req.request.method).toBe('POST');


    expect(req.request.body._id).toBe('recipe_id');
    expect(req.request.body.name).toBe('name_recipe');
    expect(req.request.body.ingredients[0].name).toBe('name_ingredient0');
    expect(req.request.body.ingredients[1].name).toBe('name_ingredient1');
    expect(req.request.body.steps[0].text).toBe('text_step0');
    expect(req.request.body.steps[1].text).toBe('text_step1');

  });

  it('should submit recipe', () => {
    component.ngOnInit();
    component.recipeId = 'recipe_id';
    component.myRecipes = [{_id: 'recipe_id',
      name: 'name_recipe',
      ingredients: [{name: 'name_ingredient0'}, {name: 'name_ingredient1'}],
      steps: [{text: 'text_step0'}, {text: 'text_step1'}]}];
    component.patchValues();
    component.onSubmitRecipe();



    const req = httpMock.expectOne(AppSettings.url_API + AppSettings.api.edit_recipe.create.change);
    expect(req.request.method).toBe('POST');


    expect(req.request.body._id).toBe('recipe_id');
    expect(req.request.body.state).toBe('validation');
    expect(component.myRecipes[0].state).toBe('validation');
    expect(req.request.body.name).toBe('name_recipe');
    expect(req.request.body.ingredients[0].name).toBe('name_ingredient0');
    expect(req.request.body.ingredients[1].name).toBe('name_ingredient1');
    expect(req.request.body.steps[0].text).toBe('text_step0');
    expect(req.request.body.steps[1].text).toBe('text_step1');

  });



  it('should publish recipe', () => {
    component.ngOnInit();
    component.recipeId = 'recipe_id';
    component.myRecipes = [{_id: 'recipe_id',
      name: 'name_recipe',
      ingredients: [{name: 'name_ingredient0'}, {name: 'name_ingredient1'}],
      steps: [{text: 'text_step0'}, {text: 'text_step1'}]}];
    component.patchValues();
    component.onPublish();



    const req = httpMock.expectOne(AppSettings.url_API + AppSettings.api.edit_recipe.create.change);
    expect(req.request.method).toBe('POST');


    expect(req.request.body._id).toBe('recipe_id');
    expect(req.request.body.state).toBe('published');
    expect(component.myRecipes[0].state).toBe('published');
    expect(req.request.body.name).toBe('name_recipe');
    expect(req.request.body.ingredients[0].name).toBe('name_ingredient0');
    expect(req.request.body.ingredients[1].name).toBe('name_ingredient1');
    expect(req.request.body.steps[0].text).toBe('text_step0');
    expect(req.request.body.steps[1].text).toBe('text_step1');

  });


  it('should cancel validation recipe', () => {
    component.ngOnInit();
    component.recipeId = 'recipe_id';
    component.myRecipes = [{_id: 'recipe_id',
      name: 'name_recipe',
      ingredients: [{name: 'name_ingredient0'}, {name: 'name_ingredient1'}],
      steps: [{text: 'text_step0'}, {text: 'text_step1'}]}];
    component.patchValues();
    component.onCancelValidation();



    const req = httpMock.expectOne(AppSettings.url_API + AppSettings.api.edit_recipe.create.change);
    expect(req.request.method).toBe('POST');


    expect(req.request.body._id).toBe('recipe_id');
    expect(req.request.body.state).toBe('creation');
    expect(component.myRecipes[0].state).toBe('creation');
    expect(req.request.body.name).toBe('name_recipe');
    expect(req.request.body.ingredients[0].name).toBe('name_ingredient0');
    expect(req.request.body.ingredients[1].name).toBe('name_ingredient1');
    expect(req.request.body.steps[0].text).toBe('text_step0');
    expect(req.request.body.steps[1].text).toBe('text_step1');

  });



});
