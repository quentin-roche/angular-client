import {Component, NgModule, OnDestroy, OnInit} from '@angular/core';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import {User} from '../models/User.model';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {IngredientService} from '../services/ingredient.service';
import {EditRecipeService} from '../services/edit-recipe.service';
import { AppSettings } from '../app.settings';
import {AuthService} from '../services/auth.service';


import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

class Ingredient {
  name = ['', Validators.required];
  quantity = ['', Validators.required];
  unit = ['', Validators.required];
}


class Step {
  text = ['', Validators.required];
}

@Component({
  selector: 'app-share-recipe',
  templateUrl: './share-recipe.component.html',
  styleUrls: ['./share-recipe.component.css']
})
export class ShareRecipeComponent implements OnInit, OnDestroy {
  faCaretUp = faCaretUp;
  faCaretDown = faCaretDown;
  faTrash = faTrash;

  // recipe
  waiting = false;
  myRecipes = [];
  myRecipesSubscription: Subscription;
  recipeId: string;

  isChanged = false;
  intervalSave: any;



  userForm: FormGroup;

  ingredientsAvailable: any[];
  ingredientSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private ingredientService: IngredientService,
              private recipeService: EditRecipeService,
              private router: Router,
              private authService: AuthService

  ) {
  }

  ngOnInit() {
    this.recipeId = this.router.url.split('/')[this.router.url.split('/').length - 1];

    this.myRecipesSubscription = this.recipeService.myRecipesSubject.subscribe(
    (myRecipes: any[]) => {
      this.myRecipes = myRecipes;
      this.patchValues();
    });
    this.recipeService.majMyRecipes();

    this.ingredientSubscription = this.ingredientService.ingredientSubject.subscribe(
      (ingredients: any[]) => {
        this.ingredientsAvailable = ingredients;

      }
    );
    this.ingredientService.majIngredient();

    this.initForm();
    this.onAddIngredient();
    this.onAddStep();

    this.userForm.valueChanges.subscribe(changes => {
      console.log('form change');
      this.isChanged = true;
    });

    this.patchValues();

    this.intervalSave = setInterval(
      () => {
        this.saveIfChanged();
      }, 30000);
  }

  ngOnDestroy() {
    clearInterval(this.intervalSave);
  }

  initForm() {
    this.userForm = this.formBuilder.group({
      _id: this.recipeId,
      name: ['' , Validators.required],
      difficulty: ['', Validators.required],
      cost: ['', Validators.required],
      ingredients: this.formBuilder.array([]),
      steps: this.formBuilder.array([])
    });
  }

  get ingredients(): FormArray {
    return this.userForm.get('ingredients') as FormArray;
  }

  onAddIngredient() {
    const newIngredientControl = this.formBuilder.control(null, null);
    this.ingredients.push(this.formBuilder.group(new Ingredient()));
  }

  onRemoveIngredient(i: number) {
    this.ingredients.removeAt(i);
  }

  get state() {
    let state = null;
    this.myRecipes.forEach(recipe => {
      if (recipe._id === this.recipeId) {
        state = recipe.state;
      }
    });
    return state;
  }

  setstate(value: string) {
    this.myRecipes.forEach(recipe => {
      if (recipe._id === this.recipeId) {
        recipe.state = value;
      }
    });
  }

  get steps(): FormArray {
    return this.userForm.get('steps') as FormArray;
  }

  onAddStep() {
    const newStepControl = this.formBuilder.control(null, null);
    this.steps.push(this.formBuilder.group(new Step()));
  }

  onRemoveStep(i: number) {
    this.steps.removeAt(i);
  }

  onUpStep(i: number) {
    const stepMove = <string> this.steps.value[i];
    console.log(stepMove);
    this.steps.at(i).patchValue({
      text: this.steps.value[i - 1]['text']
    });
    this.steps.at(i - 1).patchValue({
      text: stepMove['text']
    });
  }

  onDownStep(i: number) {
    const stepMove = this.steps.value[i];
    this.steps.at(i).patchValue({
      text: this.steps.value[i + 1]['text']
    });
    this.steps.at(i + 1).patchValue({
      text: stepMove['text']
    });
  }

  patchValues() {
    this.myRecipes.forEach( recipe => {
      if (recipe._id === this.recipeId) {
        while (this.ingredients.length < recipe.ingredients.length) {
          this.onAddIngredient();
        }
        while (this.steps.length < recipe.steps.length) {
          this.onAddStep();
        }
        this.userForm.patchValue(recipe);
        console.log(recipe);
      }
    });

  }

  onSave() {
    this.recipeService.changeRecipe(this.userForm.value);
  }
  saveIfChanged() {
    if (this.isChanged) {
      this.onSave();
    }
    this.isChanged = false;
  }

  get urlApi() {
    return AppSettings.api.images.upload.photo_recette + '/' + this.recipeId;
  }

  onSubmitRecipe() {
    this.recipeService.changeRecipe( Object.assign({}, this.userForm.value, { state: 'validation'}));
    this.setstate('validation');
    this.router.navigate(['my-recipe']);
  }

  onCancelValidation() {
    this.recipeService.changeRecipe( Object.assign({}, this.userForm.value, {state: 'creation'}));
    this.setstate('creation');
  }

  onPublish() {
    this.recipeService.changeRecipe( Object.assign({}, this.userForm.value, { state: 'published'}));
    this.router.navigate(['my-recipe']);
    this.setstate('published');
  }

  get isAdmin() {
    return this.authService.getIsAdmin();
  }
}
