import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {IngredientService} from '../services/ingredient.service';
import {Subscription} from 'rxjs/Subscription';



@Component({
  selector: 'app-modify-ingredient',
  templateUrl: './modify-ingredient.component.html',
  styleUrls: ['./modify-ingredient.component.scss']
})
export class ModifyIngredientComponent implements OnInit {
  userForm: FormGroup;

  ingredients: any[];
  ingredientSubscription: Subscription;

  constructor(
    private ingredientService: IngredientService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.initForm();
    this.ingredientSubscription = this.ingredientService.ingredientSubject.subscribe(
      (ingredients: any[]) => {
        this.ingredients = ingredients;
      }
    );
    this.ingredientService.majIngredient();
  }



  initForm() {
    this.userForm = this.formBuilder.group({
      name: ['' , Validators.required],
      type: ['' , Validators.required]
    });
  }

  onSubmitForm() {
    this.ingredientService.addIngredient(this.userForm.value.name, this.userForm.value.type);
  }
}
