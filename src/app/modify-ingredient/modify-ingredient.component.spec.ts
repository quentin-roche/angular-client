import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyIngredientComponent } from './modify-ingredient.component';
import {HttpService} from '../services/http.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NgForm, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {RecipeService} from '../services/recipe.service';
import {DndComponent} from '../dnd/dnd.component';
import {CarouselRecipeComponent} from '../carousel-recipe/carousel-recipe.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {IngredientService} from '../services/ingredient.service';

describe('ModifyIngredientComponent', () => {
  let component: ModifyIngredientComponent;
  let fixture: ComponentFixture<ModifyIngredientComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:   [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        ],
      providers: [
        HttpService,
        AuthService,
        IngredientService
      ],
      declarations: [ ModifyIngredientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyIngredientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
