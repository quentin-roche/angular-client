import { Component, OnInit } from '@angular/core';
import {EditRecipeService} from '../services/edit-recipe.service';
import {IngredientService} from '../services/ingredient.service';
import {Subscription} from 'rxjs/Subscription';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-my-recipe',
  templateUrl: './my-recipe.component.html',
  styleUrls: ['./my-recipe.component.css']
})
export class MyRecipeComponent implements OnInit {
  waiting = false;
  myRecipes = [];
  myRecipesSubscription: Subscription;
  constructor(
    private recipeService: EditRecipeService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.myRecipesSubscription = this.recipeService.myRecipesSubject.subscribe(
    (myRecipes: any[]) => {
      this.myRecipes = myRecipes;
    });
    this.recipeService.majMyRecipes();
  }

  onCreateRecipe() {
    console.log(this.waiting);
    this.waiting = true;

    this.recipeService.create();

  }


  onRemoveRecipe(recipe) {
    this.recipeService.delete(recipe);
  }

  get isAdmin() {
    return this.authService.getIsAdmin();
  }

  get userId() {
    return this.authService.getUserId();
  }
}
