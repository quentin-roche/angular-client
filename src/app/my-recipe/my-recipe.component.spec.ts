import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyRecipeComponent } from './my-recipe.component';
import {AuthService} from '../services/auth.service';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {HttpService} from '../services/http.service';
import {RouterTestingModule} from '@angular/router/testing';
import {IngredientService} from '../services/ingredient.service';
import {ConfirmationPopoverModule} from 'angular-confirmation-popover';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ReactiveFormsModule} from '@angular/forms';
import {EditRecipeService} from '../services/edit-recipe.service';
import { SearchFilterPipe } from '../Pipe/searchFilter'

describe('MyRecipeComponent', () => {
  let component: MyRecipeComponent;
  let fixture: ComponentFixture<MyRecipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:   [
        HttpClientTestingModule,
        RouterTestingModule,
        ConfirmationPopoverModule,
      ],
      providers: [
        HttpService,
        EditRecipeService,
        AuthService,
      ],
      declarations: [ MyRecipeComponent, SearchFilterPipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
