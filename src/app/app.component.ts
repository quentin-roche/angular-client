import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppareilService} from './services/appareil.service';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {AuthService} from './services/auth.service';

import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faUtensils } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  faSearch = faSearch;
  faUtensils = faUtensils;

  secondes: number;
  counterSubscription: Subscription;

  constructor(private authService: AuthService) {
  }
  ngOnInit() {
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }

  onSignOut() {
    console.log('disconnect');
    this.authService.signOut();

  }

  isAuth() {
    return this.authService.getAuthState();
  }

  isAdmin() {
    return this.authService.getIsAdmin();
  }

  onFileAdd(event: any) {
    console.log(event);
    event.handled = true;
  }

}
