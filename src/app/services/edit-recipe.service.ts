import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { AppSettings } from '../app.settings';
import {Subject} from 'rxjs/Subject';
import {Router} from '@angular/router';

@Injectable()
export class EditRecipeService {



  myRecipesSubject = new Subject<any[]>();
  private myRecipes = [];

  constructor(
    private httpClient: HttpService,
    private router: Router
  ) { }

  public create() {
    this.httpClient.post(AppSettings.api.edit_recipe.create.new, '').subscribe(
      res => {
        console.log('Demande de creation de recette');
        console.log(res);

        if (res['status'] === 'Created_successfully' ) {
          console.log('OK');

          this.router.navigate(['/share-recipe', res['_id']]);
        }
      }
    );
    this.majMyRecipes();
  }

  emitIngredientSubject() {
    this.myRecipesSubject.next(this.myRecipes.slice());
  }



  majMyRecipes() {
    this.httpClient.get(AppSettings.api.edit_recipe.create.get).subscribe(
      res => {
        console.log(res);
        this.myRecipes = res;
        this.emitIngredientSubject();
      }
    );
  }

  changeRecipe(data) {
    this.httpClient.post(AppSettings.api.edit_recipe.create.change, data).subscribe(res => {
        console.log('recipe updated');
      }
    );
  }

  delete(recipe) {
    this.httpClient.delete(AppSettings.api.edit_recipe.create.delete + '/' + recipe._id).subscribe(
      res => {
        console.log(res);
        this.myRecipes.splice(this.myRecipes.indexOf(recipe), 1);
        this.majMyRecipes();
      }
    );
  }
}
