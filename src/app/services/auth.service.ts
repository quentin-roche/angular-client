import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { AppSettings } from '../app.settings';


@Injectable()

export class AuthService {
  private isAuth = false;
  private isAdmin = false;
  private userId = 0;

  constructor(private httpService: HttpService) {
    /*this.httpService.get(AppSettings.api.auth.isAuth).subscribe(
      res => {
        console.log('isAuth ?');
        console.log(res);

        if (res['status'] === 'Connected' ) {
          if ( res['isAdmin'] ) {
            this.isAdmin = true;
          } else {
            this.isAdmin = false;
          }
          this.isAuth = true;
          console.log('connected');

        }
      },
      err => {
        console.log('Une Erreur ! : ', err);
      }
    );*/
  }

  signIn(email: String, password: String) {



    this.httpService.post(AppSettings.api.auth.connect, {email: email, password: password}).subscribe(
      res => {
        console.log('Tentative de connexion');
        console.log(res);

        if (res['status'] === 'Connected' ) {
          if ( res['isAdmin'] ) {
            this.isAdmin = true;
          } else {
            this.isAdmin = false;
          }
          this.userId = res['userId'];
          this.isAuth = true;
          console.log('connected');

        }
      },
      err => {
        console.log('Erreur ! : ', err);
      }
    );
  }


  signOut() {
   this.isAuth = false;
  }

  getAuthState() {
    return this.isAuth;
  }

  getIsAdmin() {
    return this.isAdmin;
  }


  getUserId() {
    return this.userId;
  }
}
