import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';



import { Observable } from 'rxjs/Observable';
import { AppSettings } from '../app.settings';
import { Router } from '@angular/router';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';



@Injectable()
export class HttpService {

  protected baseUrl =  AppSettings.url_API;

  constructor(private httpClient: HttpClient,
              private router: Router
  ) {  }

  get(relativeUrl: string): Observable<any> {
    return this.httpClient.get(this.baseUrl + relativeUrl, { withCredentials: true })
      .map((res: Response) => {
        console.log('>>Reponse');
        return res;
      })
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 400) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 409) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 406) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 403) {
          console.log(this.router.navigate(['/auth']));
          console.log('Forbidden');
          return Observable.throw(new Error(error.status));
        }
      });
  }

  getArrayBuffer(relativeUrl: string): Observable<any> {
    return this.httpClient.get(this.baseUrl + relativeUrl, {withCredentials: true, responseType: 'arraybuffer'})
      .map((res: any) => {
        console.log('>>Reponse');
        return res;
      })
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 400) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 409) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 406) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 403) {
          console.log(this.router.navigate(['/auth']));
          console.log('Forbidden');
          return Observable.throw(new Error(error.status));
        }
      });
  }


  post(relativeUrl: string, data: any): Observable<any> {
    return this.httpClient.post(this.baseUrl + relativeUrl, data, { withCredentials: true })
      .map((res: Response) => {
        return res;
      })
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 400) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 409) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 406) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 403) {
          console.log('Forbidden');
          return Observable.throw(new Error(error.status));
        }
      });
  }

  delete(relativeUrl: string): Observable<any> {
    return this.httpClient.delete(this.baseUrl + relativeUrl, { withCredentials: true })
      .map((res: Response) => {
        return res;
      })
      .catch((error: any) => {
        if (error.status === 500) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 400) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 409) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 406) {
          return Observable.throw(new Error(error.status));
        }
        else if (error.status === 403) {
          console.log(this.router.navigate(['/auth']));
          console.log('Forbidden');
          return Observable.throw(new Error(error.status));
        }
      });
  }


}

