import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {HttpService} from './http.service';
import { AppSettings } from '../app.settings';
import { Observable } from 'rxjs/Observable';


export class Ingredient {
  name: string;
  quantity: number;
  unit: string;
}

export class Steps {
  text: string;
}

export class Recipe {
  _id: number;
  user: number;
  name: string;
  cost: string;
  difficulty: string;
  state: string;
  ingredients: Ingredient[];
  steps: Steps[];
}

@Injectable()
export class RecipeService {


  recipeSubject = new Subject<any[]>();
  private recipe = [];


  constructor(
    private httpService: HttpService
  ) {
    this.majRecipe();
  }

  emitRecipeSubject() {
    this.recipeSubject.next(this.recipe.slice());
  }


  majRecipe() {
    this.httpService.get(AppSettings.api.recipe.get).subscribe(
      res => {
        console.log('Demande de la liste d ingredient');
        console.log(res);



        this.recipe = res;
        this.emitRecipeSubject();

      },
      err => {
        console.log('Erreur ! : ', err);
      });
  }

  getRecipe(idRecipe: string): Observable<Recipe> {
    return this.httpService.get(AppSettings.api.recipe.get + '/' + idRecipe).map( res => {
      return <Recipe> res;
    });

  }


}
