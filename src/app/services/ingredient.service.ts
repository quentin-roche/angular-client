import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {HttpClient} from '@angular/common/http';
import {HttpService} from './http.service';
import {AppSettings} from '../app.settings';

interface IngredientObj {
  name: string;
  type: string;
}


@Injectable()
export class IngredientService {



  ingredientSubject = new Subject<any[]>();
  private ingredients = [];

  constructor(private httpService: HttpService) {
    this.majIngredient();
  }

  emitIngredientSubject() {
    this.ingredientSubject.next(this.ingredients.slice());
  }

  addIngredient(name: string, type: string) {
    const ingredientObject = {
      name: '',
      type: ''
    };

    ingredientObject.name = name;
    ingredientObject.type = type;

    this.httpService.post(AppSettings.api.ingredients.create, ingredientObject).subscribe(
      res => {
        console.log('Tentative d envoi du nouvelle ingredient');
        console.log(res);

        if (res['status'] === 'Created_successfully' ) {

          this.ingredients.push(ingredientObject);
          this.emitIngredientSubject();
        }
      },
      err => {
        console.log('Erreur ! : ', err);
      }
    );
  }

  majIngredient() {
    this.httpService.get(AppSettings.api.ingredients.get).subscribe(
    res => {
      console.log('Demande de la liste d ingredient');
      console.log(res);



        this.ingredients = <IngredientObj[]> res;
        this.emitIngredientSubject();

    },
    err => {
      console.log('Erreur ! : ', err);
    });

  }

}
