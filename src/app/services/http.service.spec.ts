
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed, inject} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { HttpService } from './http.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('HttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        HttpService,
      ],
    });
  });

  it('should get users', async(inject([HttpTestingController, HttpService],
    (httpMock: HttpTestingController, apiService: HttpService) => {
      expect(apiService).toBeTruthy();
    }
    ))
  );
});
