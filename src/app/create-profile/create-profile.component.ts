import { Component, OnInit } from '@angular/core';
import {AppareilService} from '../services/appareil.service';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {AuthComponent} from '../auth/auth.component';
import {HttpService} from '../services/http.service';
import {AppSettings} from '../app.settings';

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss']
})
export class CreateProfileComponent implements OnInit {


  userForm: FormGroup;


  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private httpService: HttpService
  ) { }

  ngOnInit() {
    this.initForm();
  }



  initForm() {
    this.userForm = this.formBuilder.group({
      email: ['' , Validators.required],
      password: ['' , Validators.required],
      passwordVerif: ['' , Validators.required],
      firstName: ['' , Validators.required],
      lastName: ['' , Validators.required],
    });
  }

  onSubmit() {
    this.httpService.post(AppSettings.api.auth.create,  this.userForm.value).subscribe(
      res => {
        console.log('Création réussie');
        console.log(res);
        this.router.navigate(['/']);
      }
    );
  }


}
