import {async, ComponentFixture, getTestBed, TestBed} from '@angular/core/testing';

import { CreateProfileComponent } from './create-profile.component';
import {HttpService} from '../services/http.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NgForm, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {AppSettings} from '../app.settings';

describe('CreateProfileComponent', () => {
  let component: CreateProfileComponent;
  let fixture: ComponentFixture<CreateProfileComponent>;



  let httpMock: HttpTestingController;
  let injector: TestBed;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:   [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule
        ],
      providers: [
        HttpService,
        AuthService
      ],
      declarations: [ CreateProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });



  it( 'should create profile', () => {
    const formValues = {email: 'email@domaine', password: 'passs', passwordVerif: 'pass', firstName: 'Quentin', lastName: 'Roche'}
    component.userForm.value.email = formValues.email;
    component.userForm.value.password = formValues.password;
    component.userForm.value.passwordVerif = formValues.passwordVerif;
    component.userForm.value.firstName = formValues.firstName;
    component.userForm.value.lastName = formValues.lastName;


    component.onSubmit();


    const req = httpMock.expectOne(AppSettings.url_API + AppSettings.api.auth.create);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.email).toBe(formValues.email);
    expect(req.request.body.password).toBe(formValues.password);
    req.flush(formValues);
  });
});
