import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

import { AppComponent } from './app.component';
import { AppareilService} from './services/appareil.service';
import { AuthComponent } from './auth/auth.component';
import { RouterModule, Routes} from '@angular/router';
import { AuthService} from './services/auth.service';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { AuthGuard} from './services/auth-guard.services';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CreateProfileComponent } from './create-profile/create-profile.component';
import { HomeComponent } from './home/home.component';
import { ShareRecipeComponent} from './share-recipe/share-recipe.component';
import { AdminGuard} from './services/admin-guard.service';
import { ModifyIngredientComponent } from './modify-ingredient/modify-ingredient.component';
import { IngredientService} from './services/ingredient.service';
import { MyRecipeComponent } from './my-recipe/my-recipe.component';
import { EditRecipeService } from './services/edit-recipe.service';
import { HttpService } from './services/http.service';
import { DndComponent } from './dnd/dnd.component';
import { DndDirective } from './dnd/dnd.directive';
import { SearchFilterPipe } from './Pipe/searchFilter';
import { RecipeService } from './services/recipe.service';
import { CarouselRecipeComponent } from './carousel-recipe/carousel-recipe.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RecipeConsultComponent } from './recipe-consult/recipe-consult.component';





const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'create-profile', component: CreateProfileComponent },
  { path: 'consult-recipe/:id', component: RecipeConsultComponent },

  { path: 'my-recipe', canActivate: [AuthGuard], component: MyRecipeComponent },
  { path: 'share-recipe/:id', canActivate: [AuthGuard], component: ShareRecipeComponent },
  { path: 'modify-ingredient', canActivate: [AdminGuard], component: ModifyIngredientComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    FourOhFourComponent,
    CreateProfileComponent,
    HomeComponent,
    ShareRecipeComponent,
    ModifyIngredientComponent,
    MyRecipeComponent,
    DndComponent,
    DndDirective,
    SearchFilterPipe,
    CarouselRecipeComponent,
    RecipeConsultComponent
  ],
  imports: [
    FontAwesomeModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'}),
    NgbModule.forRoot()
  ],
  providers: [
    AppareilService,
    AuthService,
    AuthGuard,
    AdminGuard,
    IngredientService,
    EditRecipeService,
    HttpService,
    RecipeService,
  ],
  bootstrap: [
    AppComponent
  ],

})
export class AppModule { }
