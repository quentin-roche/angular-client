import {async, ComponentFixture, getTestBed, TestBed} from '@angular/core/testing';

import { CarouselRecipeComponent } from './carousel-recipe.component';
import {HttpService} from '../services/http.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppSettings} from '../app.settings';

describe('CarouselRecipeComponent', () => {
  let component: CarouselRecipeComponent;
  let fixture: ComponentFixture<CarouselRecipeComponent>;



  let httpMock: HttpTestingController;
  let injector: TestBed;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:   [
        RouterTestingModule,
        HttpClientTestingModule,
        ],
      providers: [
        HttpService,
      ],
      declarations: [ CarouselRecipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {

    fixture = TestBed.createComponent(CarouselRecipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    component.recipeId = 'repice_id';
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });


  it('should get images ids', () => {
    component.ngOnInit();
    const req = httpMock.expectOne(AppSettings.url_API + AppSettings.api.images.get.photo_recette + '/' + 'repice_id');
    expect(req.request.method).toBe('GET');
    req.flush([{_id: 'image_id'}]);
    expect(component.toLoad[0]).toBe('/api/images/photo-recette/public/repice_id/image_id');
  });

  it('should get image', () => {
    component.toLoad = ['/api/images/photo-recette/public/repice_id/image_id'];
    component.getFirstFile()
    const req = httpMock.expectOne(AppSettings.url_API + AppSettings.api.images.get.photo_recette + '/repice_id/image_id');
    expect(req.request.method).toBe('GET');
    const image = new ArrayBuffer(8);
    const uint8 = new Uint8Array(image);
    uint8.set([11, 24, 13], 3);
    req.flush(image);
    expect(component.srcImages[0]).toBe('data:image/jpeg;base64,AAAACxgNAAA=');
  });

  it('should change image', () => {
    component.srcImages[0] = 'data:image/jpeg;base64,AAAACxgNAAA=';
    component.srcImages[1] = 'data:image/jpeg;base64,AAAACxgNAAA=';
    component.changeShow();
    expect(component.indexImageDisplayed).toEqual( 1);
    component.changeShow();
    expect(component.indexImageDisplayed).toEqual( 0);
  });
})
