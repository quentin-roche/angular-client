import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { AppSettings} from '../app.settings';
import {map} from 'rxjs/operators';

import {HttpService} from '../services/http.service';


@Component(
  {selector: 'app-carousel-recipe',
    styleUrls: ['./carousel-recipe.component.css'],
    templateUrl: './carousel-recipe.component.html'})


export class CarouselRecipeComponent implements OnInit, OnDestroy {
  @Input() recipeId: string;

  toLoad = [];

  srcImages = [];
  interval: any;
  indexImageDisplayed = 0;


  constructor(
    private httpService: HttpService) {}

  ngOnInit() {
    this.httpService.get(AppSettings.api.images.get.photo_recette + '/' + this.recipeId).subscribe(
      res => {
        console.log(res);
        const r = <any[]> res;
        r.forEach((f: any) => {
          this.toLoad.push(AppSettings.api.images.get.photo_recette + '/' + this.recipeId + '/' + f._id);
        });
        this.getFirstFile();
      });
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  _arrayBufferToBase64( buffer ) {
    let binary = '';
    const bytes = new Uint8Array( buffer );
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode( bytes[ i ] );
    }
    return window.btoa( binary );
  }


  getFirstFile() {
    this.httpService.getArrayBuffer(this.toLoad[0]).subscribe(
      data => {
        console.log('image recu');
        console.log(data);
        this.srcImages.push( 'data:image/jpeg;base64,' + this._arrayBufferToBase64(data));
        this.toLoad.splice(0, 1);

        if (this.toLoad.length !== 0) {
          this.getFirstFile();
        } else {

          this.interval = setInterval(() => {
            this.changeShow();
          }, 2000);
        }
      }
    );
  }

  changeShow() {
    console.log(this.indexImageDisplayed);
    if (this.indexImageDisplayed >= this.srcImages.length - 1) {
      this.indexImageDisplayed = 0;
    } else {
      this.indexImageDisplayed = this.indexImageDisplayed + 1;
    }
    console.log(this.indexImageDisplayed);
  }

}
