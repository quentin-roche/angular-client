import { Component, OnInit } from '@angular/core';
import {RecipeService} from '../services/recipe.service';
import { faDollarSign } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  faDollarSign = faDollarSign;
  faStar = faStar;

  recipes: any[];
  recipesSubscription: any;


  constructor(
    private recipeService: RecipeService,
    private router: Router
  ) {
      this.recipesSubscription = this.recipeService.recipeSubject.subscribe(
        (recipes: any[]) => {
          this.recipes = recipes;
        });
      this.recipeService.majRecipe();
  }

  ngOnInit() {
  }

  onConsultRecipe(recipe) {
    this.router.navigate(['/consult-recipe/', recipe._id]);
  }
}
