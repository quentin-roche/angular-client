import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeConsultComponent } from './recipe-consult.component';
import {HttpService} from '../services/http.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NgForm, ReactiveFormsModule} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {RecipeService} from '../services/recipe.service';
import {DndComponent} from '../dnd/dnd.component';
import {CarouselRecipeComponent} from '../carousel-recipe/carousel-recipe.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

describe('RecipeConsultComponent', () => {
  let component: RecipeConsultComponent;
  let fixture: ComponentFixture<RecipeConsultComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:   [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        ],
      providers: [
        HttpService,
        AuthService,
        RecipeService
      ],
      declarations: [ RecipeConsultComponent, CarouselRecipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeConsultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
