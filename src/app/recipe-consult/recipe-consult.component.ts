import { Component, OnInit } from '@angular/core';
import {Recipe, RecipeService} from '../services/recipe.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-recipe-consult',
  templateUrl: './recipe-consult.component.html',
  styleUrls: ['./recipe-consult.component.css']
})
export class RecipeConsultComponent implements OnInit {
  recipeId: string;
  recipe = new Recipe();

  constructor(
    private recipeService: RecipeService,
    private router: Router
  ) {
  }

  ngOnInit() {

    this.recipeId = this.router.url.split('/')[this.router.url.split('/').length - 1];

    console.log('lecture recette');
   this.recipeService.getRecipe(this.recipeId).subscribe(
      res => {
        this.recipe = res;
      }
    );
  }

}
