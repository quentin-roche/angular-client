



export class AppSettings {
  public static url_API = 'http://cookiiing.com:8080';
  public static url = 'http://cookiiing.com:4200';

  public static api = {
    auth: {
      isAuth: '/api/user/isAuth',
      connect: '/api/user/connect',
      disconnect: '/api/user/isAuth',
      create: '/api/user/create',
      },
    edit_recipe: {
      create : {
        new: '/api/recipe/edit/new',
        get: '/api/recipe/edit/',
        change: '/api/recipe/edit/change',
        delete: '/api/recipe/edit'
      }
    },
    ingredients: {
      create: '/api/ingredient/create',
      get: '/api/ingredient',
    },
    recipe: {
      get: '/api/recipe/public',
    },
    images: {
      upload: {
        photo_recette: '/api/images/photo-recette/edit'
      },
      get: {
        photo_recette: '/api/images/photo-recette/public'
      }
    }
  };
}
