import {async, ComponentFixture, getTestBed, TestBed} from '@angular/core/testing';

import { AuthComponent } from './auth.component';
import {HttpService} from '../services/http.service';
import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {FormBuilder, NgForm, ReactiveFormsModule, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {AppSettings} from '../app.settings';

describe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;


  let httpMock: HttpTestingController;
  let injector: TestBed;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:   [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule
        ],
      providers: [
        HttpService,
        AuthService
      ],
      declarations: [ AuthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });



  it('should create', () => {
    expect(component).toBeDefined();
  });


  it( 'should signIn', () => {
    const formValues = {email: 'email@domaine', password: 'pass'}
    component.userForm.value.email = formValues.email;
    component.userForm.value.password = formValues.password;

    component.onSubmitForm();


    const req = httpMock.expectOne(AppSettings.url_API + AppSettings.api.auth.connect);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.email).toBe(formValues.email);
    expect(req.request.body.password).toBe(formValues.password);
    req.flush(formValues);
  });
});
