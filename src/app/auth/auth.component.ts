import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AuthService} from '../services/auth.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  userForm: FormGroup;

  constructor(
              private router: Router,
              private authService: AuthService,
              private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.userForm = this.formBuilder.group({
      email: [null , Validators.required],
      password: [null , Validators.required]});
  }

  onSubmitForm() {
    this.authService.signIn( this.userForm.value.email,  this.userForm.value.password);
    this.router.navigate(['/']);
  }
}
