import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HttpService} from '../services/http.service';
import { faUpload } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-dnd',
  templateUrl: './dnd.component.html',
  styleUrls: ['./dnd.component.css']
})
export class DndComponent implements OnInit {
  faUpload = faUpload;

  @Input() allowedExtensions: Array<string> = [];
  @Input() filesUrlApi: string;
  @Input() disabled: boolean;

  @Output() fileAdd = new EventEmitter<any>();
  @Output() fileRemove = new EventEmitter<any>();



  files: any = [];
  constructor(
    private httpService: HttpService,
  ) {  }

  ngOnInit() {
    this.httpService.get(this.filesUrlApi).subscribe(
      res => {
        console.log('get file');
        let fs: Array<any>;
        fs = res;
        console.log(fs);
        fs.forEach((f: any) => {
          console.log(f.name);
          const file = {file: {name: f.name}, src: null, id: null};
          this.files.push(file);

          const ext = file.file.name.split('.')[ file.file.name.split('.').length - 1]
          if (ext === 'png' || ext === 'jpg' || ext === 'gif') {
            this.getFiles(file, f._id);
          } else {
            file.id =  f._id;
            file.src = 'file';
          }
        });
      });
  }


  _arrayBufferToBase64( buffer ) {
    let binary = '';
    const bytes = new Uint8Array( buffer );
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode( bytes[ i ] );
    }
    return window.btoa( binary );
  }


  getFiles(file, fileId) {
    this.httpService.getArrayBuffer(this.filesUrlApi + '/' + fileId).subscribe(
      data => {
        console.log(data);
        file.src = 'data:image/jpeg;base64,' + this._arrayBufferToBase64(data)
        file.id = fileId;

        console.log(this.files);
      }
    );
  }

  onFileDrop(files: File[]) {
    if (this.disabled) { return; }

    console.log('onFileDrop');

    console.log('files');
    console.log(files);

    if (files.length > 0) {
      console.log('>0');
      Array.from(files).forEach((file: File) => {
        const ext = file.name.split('.')[file.name.split('.').length - 1];
        if (this.allowedExtensions.lastIndexOf(ext) !== -1) {


          this.files.push({file: file, src: null, id: null});
          const file_ = this.files[this.files.length - 1];
          const files_ = this.files;
          const fileAdd_ = this.fileAdd;

          this.uploadFile(file_);

          if (ext === 'png' || ext === 'jpg' || ext === 'gif') {
            const reader = new FileReader();
            reader.addEventListener('load', function () {
              file_.src = reader.result;
            }, false);
            reader.readAsDataURL(file);
          } else {
            file_.src = 'file';
          }
        }
      });
    }
  }


  getFileExtension(file: any) {
    return file.name.split('.')[file.name.split('.').length - 1];
  }

  onChangeFileInput(event) {
    this.onFileDrop(event.srcElement.files);
    console.log(event.srcElement.files);
  }


  uploadFile(file) {
    const formData: FormData = new FormData();
    formData.append('uploadFile', file.file, file.file.name);

    this.httpService.post(this.filesUrlApi, formData).subscribe(
      res => {
        console.log('Upload' + res.id);
        file.id = res.id;
        this.fileAdd.emit(file.id);
      });
  }


  onRemoveFile(file) {
    this.httpService.delete(this.filesUrlApi + '/' + file.id).subscribe(
      res => {
        console.log('Delete');
        console.log(this.files);
        this.files.splice(this.files.indexOf(file), 1);
        this.fileRemove.emit(file.id);
      });
  }
}
