/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, getTestBed, TestBed} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DndComponent } from './dnd.component';
import {HttpService} from '../services/http.service';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { faUpload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {AppSettings} from '../app.settings';

describe('DndComponent', () => {
  let component: DndComponent;
  let fixture: ComponentFixture<DndComponent>;



  let httpMock: HttpTestingController;
  let injector: TestBed;



  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:   [
        HttpClientTestingModule,
        RouterTestingModule,
        FontAwesomeModule,
      ],
      providers: [
        HttpService,
      ],
      declarations: [ DndComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    component.filesUrlApi = '/api/images/photo-recette/edit/repice_id';
    component.allowedExtensions = ['txt', 'png'];
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });



  it('should get file info', () => {
    component.ngOnInit();
    const req = httpMock.expectOne(AppSettings.url_API + '/api/images/photo-recette/edit/repice_id');
    expect(req.request.method).toBe('GET');
    req.flush([{_id: 'file_id', name: 'file_name.txt'}]);

    expect(component.files[0].file.name).toBe('file_name.txt');
  });

  it('should get image src', () => {
    component.ngOnInit();
    const req = httpMock.expectOne(AppSettings.url_API + '/api/images/photo-recette/edit/repice_id');
    expect(req.request.method).toBe('GET');
    req.flush([{_id: 'file_id', name: 'file_name.jpg'}]);

    const reqSrc = httpMock.expectOne(AppSettings.url_API + '/api/images/photo-recette/edit/repice_id/file_id');
    expect(reqSrc.request.method).toBe('GET');

    const image = new ArrayBuffer(8);
    const uint8 = new Uint8Array(image);
    uint8.set([11, 24, 13], 3);
    reqSrc.flush(image);
    expect(component.files[0].src).toBe('data:image/jpeg;base64,AAAACxgNAAA=');
  });

  it( 'should add file on drop and upload', () => {
    const fileData = new ArrayBuffer(8);
    const uint8 = new Uint8Array(fileData);
    uint8.set([11, 24, 13], 3);
    const file = new File(['coucou'], 'file_name.txt');
    component.onFileDrop([file]);
    expect(component.files[0].file.name).toBe('file_name.txt');
    expect(component.files[0].src).toBe('file');

    const req = httpMock.expectOne(AppSettings.url_API + '/api/images/photo-recette/edit/repice_id');
    expect(req.request.method).toBe('POST');

  });

  it( 'should read image on drop', () => {
    const fileData = new ArrayBuffer(8);
    const uint8 = new Uint8Array(fileData);
    uint8.set([11, 24, 13], 3);
    const file = new File([fileData], 'file_name.png');
    component.onFileDrop([file]);

    expect(component.files[0].file.name).toBe('file_name.png');
    expect(component.files[0].src).toBe(null);
  });

  it( 'should read file extension', () => {
    const fileData = new ArrayBuffer(8);
    const uint8 = new Uint8Array(fileData);
    uint8.set([11, 24, 13], 3);
    const file = new File([fileData], 'file_name.png');
    expect(component.getFileExtension(file)).toBe('png');
  });

  it( 'should delet file', () => {
    component.files = [{file: {name: 'file_name.jpg'}, src: 'file', id: 'file_id'}];
    component.onRemoveFile( component.files[0] );


    const req = httpMock.expectOne(AppSettings.url_API + '/api/images/photo-recette/edit/repice_id/file_id');
    expect(req.request.method).toBe('DELETE')
    req.flush({});


    expect(component.files.length).toBe(0);
  });

});
