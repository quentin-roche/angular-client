import {Directive, HostListener, HostBinding, EventEmitter, Output, Input} from '@angular/core';


@Directive({
  selector: '[appDnd]'
})
export class DndDirective {
  @Input() private allowed_extensions: Array<string> = [];
  @Output() private filesEmiter: EventEmitter<any> = new EventEmitter();


  @Output() private filesDropEmiter: EventEmitter<File[]> = new EventEmitter();
  @HostBinding('style.background') private background = '#eee';


  valid_files: Array<File> = [];
  invalid_files: Array<File> = [];
  srcList:  Array<any> = [];


  files:  Array<any> = [];

  constructor() { }

  @HostListener('dragover', ['$event']) public onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#999';
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#eee';
  }

  @HostListener('drop', ['$event']) public onDrop(evt) {
    console.log('drop');
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#eee';
    const files = evt.dataTransfer.files;
    this.filesDropEmiter.emit(files);
  }

}
